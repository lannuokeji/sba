package com.lannuokeji.sba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
/**
 * 核心启动类
 * @author zzu
 *
 */
@SpringBootApplication
@EnableScheduling
@EnableAdminServer
public class MainApplication {
	public static void main(String[] args) throws Exception {
        SpringApplication.run(MainApplication.class, args);
    }
}
